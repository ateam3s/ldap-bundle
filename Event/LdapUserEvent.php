<?php

namespace IMAG\LdapBundle\Event;

use IMAG\LdapBundle\User\LdapUserInterface;
use Symfony\Component\EventDispatcher\Event;

class LdapUserEvent extends Event
{
    private $user;

    public function __construct(LdapUserInterface $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
